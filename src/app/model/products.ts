export class Product{
    id: number;
    name: string;
    cost: number;
    description: string;   
    type: string;
}