import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Product} from '../model/products';
import { from } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ListProductService {

  mockApi;

  constructor(private http: HttpClient) {
    this.mockApi = environment.urlApi;
   }

   getData(){
     return this.http.get(this.mockApi);
   }

   createProduct(product: Product){
    return this.http.post(this.mockApi, product);
   }

  //  updateProduct(product: Product){
  //    return this.http.put();
  //  }
}
