import { Component, OnInit } from '@angular/core';
import { ListProductService } from '../list-product.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {

  constructor(
    private ListProductService: ListProductService,
    private fb: FormBuilder,
    private router: Router,
  ) { }

  addProduct: FormGroup;

  ngOnInit() {
    this.addProduct = this.fb.group({
      id: [''],
      name: [''],
      cost: [''],
      description: [''],
      type: ['']
    });
  }

  onSubmit() {
    console.log(this.addProduct);
    this.ListProductService.createProduct(this.addProduct.value).subscribe(data => {
      this.router.navigate(['listProduct']);

    })
  }
}




