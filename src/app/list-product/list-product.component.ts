import { Component, OnInit } from '@angular/core';
import {ListProductService} from './list-product.service'
@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss']
})
export class ListProductComponent implements OnInit {


  listproduct = [];

  constructor(
    private ListProductService: ListProductService
  ) {

  } 

  ngOnInit() {
    this.ListProductService.getData().subscribe((data: any[]) =>{
      console.log(data);
      this.listproduct = data;
    })
  }

}
