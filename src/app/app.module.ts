import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { CrmComponent } from './crm/crm.component';
import { ListProductComponent } from './list-product/list-product.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { UpdateProductComponent } from './list-product/update-product/update-product.component';
import {NewProductComponent} from './list-product/new-product/new-product.component'

const appRoutes: Routes = [
  {path: '', component: CrmComponent},
  {path: 'listProduct', component:ListProductComponent},
  {path: 'editProduct', component: UpdateProductComponent},
  {path: 'newProduct', component: NewProductComponent}
]
@NgModule({
  declarations: [
    AppComponent,
    CrmComponent,
    ListProductComponent,
    NavbarComponent,
    SidebarComponent,
    UpdateProductComponent,
    NewProductComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule, ReactiveFormsModule, 
    NgxPaginationModule,
      RouterModule.forRoot(
        appRoutes,
        { enableTracing: true } // <-- debugging purposes only
      )
      // other imports here
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
