
window.onload = function () {

    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        axisX: {
            tickThickness: 0,
            labelFontColor: "#CCCDD3",
            valueFormatString: "MMM",
            lineColor: "darkgrey",
        },
        axisY: [{
            tickThickness: 0,
            labelFontColor: "#CCCDD3",
            valueFormatString: "0",
            lineThickness: 0,
            suffix: "$",
            gridDashType: "dot",
            gridColor: "darkgrey",

        }],
        data: [{
            type: "splineArea",
            color: "#456BD8",
            makeSize: 1,
            yValueFormatString: "##,### $",
            xValueFormatString: "MMM",
            
            dataPoints: [
                { x: new Date(2019, 0), y: 0 },
                { x: new Date(2019, 1), y: 10000 },
                { x: new Date(2019, 2), y: 5000 },
                { x: new Date(2019, 3), y: 15000 },
                { x: new Date(2019, 4), y: 10000 },
                { x: new Date(2019, 5), y: 20000 },
                { x: new Date(2019, 6), y: 15000 },
                { x: new Date(2019, 7), y: 25000 },
                { x: new Date(2019, 8), y: 20000 },
                { x: new Date(2019, 9), y: 30000 },
                { x: new Date(2019, 10), y: 25000 },
                { x: new Date(2019, 11), y: 40000 },
            ]
        }]
    });
    chart.render();

    var chart = new CanvasJS.Chart("chartDoughnut", {

        exportFileName: "Doughnut Chart",
        exportEnabled: true,
        animationEnabled: true,
        // title: {
        //     text: "Monthly Expense"
        // },
        // legend: {
        //     cursor: "pointer",
        //     itemclick: explodePie
        // },
        data: [{
            type: "doughnut",
            innerRadius: 110,
            showInLegend: true,

            dataPoints: [
                { y: 60, name: "Direct" },
                { y: 25, name: "Social" },
                { y: 15, name: "Referral" },

            ]
        }]
    });
    chart.render();

    function explodePie(e) {
        if (typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
            e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
        } else {
            e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
        }
        e.chart.render();
    }

}

